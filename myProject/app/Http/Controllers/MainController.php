<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function home() {
        return view('home');
    }

    public function buy($idItem) {
        return view('buy', ['idItem' => $idItem]);
    }

    public function buy_check(Request $request) {

        $valid = $request->validate([
                'email' => 'required|min:4|max:100',
                'firstName' => 'required|min:4|max:100',
                'lastName' => 'required|min:4|max:100',
        ]);

        $buing = new Contact();
        $buing->email = $request->input('email');
        $buing->firstName = $request->input('firstName');
        $buing->lastName = $request->input('lastName');
        $buing->idItem = $request->input('idItem');
        $buing->save();

        return redirect()->route('home');
    }

    public function sells() {
        $sells = new Contact();

        //return dd($sells->all());

        return view('sells', ['sells' => $sells->all()]);
    }
}
