@extends('header')

@section('main_content')

    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Id Item</th>
            <th>Дата</th>
        </tr>
        </thead>
        <tbody>
        @php( $n = 0 )
        @foreach($sells as $el)
            <tr>
                <th scope="row">{{($n++) + 1}}</th>
                <th scope="row">{{$el->id}}</th>
                <td>{{$el->firstName}}</td>
                <td>{{$el->lastName}}</td>
                <td>{{$el->email}}</td>
                <td>{{$el->idItem}}</td>
                <td>{{$el->created_at}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
@endsection
