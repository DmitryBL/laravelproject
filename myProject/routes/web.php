<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home')->name('home');

Route::get('/buy/{idItem}', 'MainController@buy');

Route::post('/buy/check', 'MainController@buy_check');

Route::get('/sells', 'MainController@sells');





Route::get('/user/{id}/{name}', function ($id, $name) {
    return 'ID: ' . $id . ' Name: ' . $name;
});

//Route::get('/', [Controller::class, 'welcome'] );
